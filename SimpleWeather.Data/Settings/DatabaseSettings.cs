namespace SimpleWeather.Data.Settings
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
        public bool InMemory { get; set; }
    }
}