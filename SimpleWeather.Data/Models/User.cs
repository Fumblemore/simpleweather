﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleWeather.Data.Models
{
    [Table("User")]
    public class User
    {
        public int Id { get;  set; }
        public string Login { get; protected set; }
        public string Hash { get; protected set; }
        public string Salt { get; protected set; }
        public Role Role { get; protected set; }

        protected User() { }

        public User(string login, 
            string hash, string salt, Role role)
        {
            Login = login;
            Hash = hash;
            Salt = salt;
            Role = role;
        }
    }
}
