﻿using Autofac;
using Microsoft.Extensions.Configuration;
using SimpleWeather.Data.Repositories;
using SimpleWeather.Data.Settings;

namespace SimpleWeather.Data.IoC
{
    public class DataModule : Module
    {
        private readonly IConfiguration _configuration;

        public DataModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherDownloader>()
                .As<IWeatherDownloader>()
                .InstancePerLifetimeScope();

            builder.RegisterType<WeatherRepository>()
                .As<IWeatherRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<WeatherContext>()
                .As<WeatherContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterInstance(_configuration.GetSettings<WeatherApiSettings>());
            builder.RegisterInstance(_configuration.GetSettings<DatabaseSettings>());
        }
    }
}
