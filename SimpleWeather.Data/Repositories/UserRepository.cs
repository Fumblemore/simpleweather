﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleWeather.Data.Models;

namespace SimpleWeather.Data.Repositories
{
    public interface IUserRepository
    {
        Task AddAsync(User user);
        bool UserExist(string login);
        Task<User> GetByLoginAsync(string login);
    }

    public class UserRepository : IUserRepository
    {
        private readonly WeatherContext _context;

        public UserRepository(WeatherContext context)
        {
            _context = context;
        }

        public async Task AddAsync(User user)
        {
            await _context.UserDbSet.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetByLoginAsync(string login)
            => await _context
                .UserDbSet
                .SingleOrDefaultAsync(u => u.Login == login);

        public bool UserExist(string login) 
            => _context
                .UserDbSet
                .Any(u => u.Login == login);
    }
}
