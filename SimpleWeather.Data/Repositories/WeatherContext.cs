using System;
using Microsoft.EntityFrameworkCore;
using SimpleWeather.Data.Models;
using SimpleWeather.Data.Settings;

namespace SimpleWeather.Data.Repositories
{
    public sealed class WeatherContext : DbContext
    {
        private readonly DatabaseSettings _databaseSettings;

        public DbSet<Weather> WeatherDbSet { get; set; }
        public DbSet<User> UserDbSet { get; set; }

        public WeatherContext(DatabaseSettings databaseSettings)
        {          
            _databaseSettings = databaseSettings;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_databaseSettings.InMemory)
            {
                optionsBuilder.UseInMemoryDatabase("in-memory-database");
                return;
            }

            optionsBuilder.UseSqlServer(_databaseSettings.ConnectionString);
        }
    }
}