using System.Threading.Tasks;
using RestSharp;
using SimpleWeather.Data.Models;
using SimpleWeather.Data.Settings;

namespace SimpleWeather.Data.Repositories
{
    public interface IWeatherDownloader
    {
        Task<IRestResponse<WeatherApiModel>> GetAsync(string city);
    }
    public class WeatherDownloader : IWeatherDownloader
    {
        private readonly IRestClient _restClient;
        private readonly IRestRequest _restRequest;
        private readonly WeatherApiSettings _weatherApiSettings;

        public WeatherDownloader(WeatherApiSettings weatherApiSettings)
        {
            _weatherApiSettings = weatherApiSettings;

            _restClient = new RestClient(_weatherApiSettings.BaseUrl);
            _restRequest = new RestRequest(_weatherApiSettings.Resource);
            _restRequest.AddParameter(_weatherApiSettings.ApiKeyParameter, _weatherApiSettings.ApiKeyValue);
            _restRequest.AddParameter("units", "metric");
        }

        public async Task<IRestResponse<WeatherApiModel>> GetAsync(string city)
        {
            _restRequest.AddParameter(_weatherApiSettings.CityParameter, city);
            return await _restClient
                .ExecuteTaskAsync<WeatherApiModel>(_restRequest);
        }
    }
}