using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleWeather.Data.Models;

namespace SimpleWeather.Data.Repositories
{
    public interface IWeatherRepository
    {
        Task<bool> Add(Weather weather);
        Task<Weather> GetAsync(string city);
    }

    public class WeatherRepository : IWeatherRepository
    {
        private readonly WeatherContext _context;

        public WeatherRepository(WeatherContext context)
        {
            _context = context;
        }

        public async Task<bool> Add(Weather weather)
        {
            weather.City = weather.City.ToLower();
            await _context.WeatherDbSet.AddAsync(weather);
            return await _context.SaveChangesAsync() == 1;
        }

        public Task<Weather> GetAsync(string city)
        {
            return _context
                .WeatherDbSet
                .Where(w => w.City == city)
                .OrderByDescending(w => w.DateTime)
                .FirstOrDefaultAsync();
        }
    }
}