﻿namespace SimpleWeather.Infrastructure.Dto
{
    public enum UserRegisterResponse
    {
        Ok,
        UserExist,
        PasswordEmpty,
    }
}
