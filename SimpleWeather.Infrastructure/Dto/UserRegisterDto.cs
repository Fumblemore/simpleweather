﻿namespace SimpleWeather.Infrastructure.Dto
{
    public class UserRegisterDto
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
