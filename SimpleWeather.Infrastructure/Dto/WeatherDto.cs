﻿using System;

namespace SimpleWeather.Infrastructure.Dto
{
    public class WeatherDto
    {
        public string City { get; set; }
        public string WeatherType { get; set; }
        public float Temperature { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
        public float WindSpeed { get; set; }
        public int WindDegree { get; set; }
        public int CloudsPercentage { get; set; }
        public int Snow { get; set; }
        public int Rain { get; set; }
    }
}
