﻿using System;

namespace SimpleWeather.Infrastructure.Extensions
{
    public static class TypeExtensions
    {
        public static TOut DefaultIfNull<TIn, TOut>(this TIn value, Func<TIn, TOut> geter) 
            => value == null ? default(TOut) : geter(value);

        //Syntactic sugar
        public static bool IsNullOrEmpty(this string value)
            => string.IsNullOrEmpty(value);

        public static int ToTimeStamp(this DateTime dateTime)
            => (int) dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
    }
}
