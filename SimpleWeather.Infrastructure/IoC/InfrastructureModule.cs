﻿using Autofac;
using Microsoft.Extensions.Configuration;
using SimpleWeather.Data.IoC;
using SimpleWeather.Data.Settings;
using SimpleWeather.Infrastructure.Mappers;
using SimpleWeather.Infrastructure.Services;
using SimpleWeather.Infrastructure.Settings;

namespace SimpleWeather.Infrastructure.IoC
{
    public class InfrastructureModule : Module
    {
        private readonly IConfiguration _configuration;

        public InfrastructureModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherService>()
                .As<IWeatherService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserService>()
                .As<IUserService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EncryptService>()
                .As<IEncryptService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<JwtService>()
                .As<IJwtService>()
                .SingleInstance();

            builder.RegisterType<SeedUserService>()
                .As<IStartable>()
                .AutoActivate();

            builder.RegisterInstance(AutoMapperConfig.Create());
            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>());

            builder.RegisterModule(new DataModule(_configuration));
        }
    }
}
