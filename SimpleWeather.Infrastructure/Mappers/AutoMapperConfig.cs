﻿using System.Linq;
using AutoMapper;
using SimpleWeather.Data.Models;
using SimpleWeather.Infrastructure.Dto;
using SimpleWeather.Infrastructure.Extensions;

namespace SimpleWeather.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Create() 
            => new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<WeatherApiModel, Weather>()
                        .ForMember(dest => dest.Id, opt => opt.Ignore())
                        .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.Name))
                        .ForMember(dest => dest.WeatherType,
                            opt => opt.MapFrom(src => src.Weather.FirstOrDefault().DefaultIfNull(w => w.Main)))
                        .ForMember(dest => dest.Temperature, opt => opt.MapFrom(src => src.Main.Temp))
                        .ForMember(dest => dest.Pressure, opt => opt.MapFrom(src => src.Main.Pressure))
                        .ForMember(dest => dest.Humidity, opt => opt.MapFrom(src => src.Main.Humidity))
                        .ForMember(dest => dest.WindSpeed, opt => opt.MapFrom(src => src.Wind.Speed))
                        .ForMember(dest => dest.WindDegree, opt => opt.MapFrom(src => src.Wind.Deg))
                        .ForMember(dest => dest.CloudsPercentage, opt => opt.MapFrom(src => src.Clouds.All))
                        .ForMember(dest => dest.Rain, 
                            opt => opt.MapFrom(src => src.Rain.DefaultIfNull(rain => rain.Rain3h)))
                        .ForMember(dest => dest.Snow, 
                            opt => opt.MapFrom(src => src.Snow.DefaultIfNull(rain => rain.Snow3h)));

                    cfg.CreateMap<Weather, WeatherDto>();
                })
                .CreateMapper();
    }
}
