using System;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using RestSharp;
using SimpleWeather.Data.Models;
using SimpleWeather.Data.Repositories;
using SimpleWeather.Infrastructure.Dto;

namespace SimpleWeather.Infrastructure.Services
{
    public interface IWeatherService
    {
        Task<RefreshResponse> Refresh(string city);
        Task<WeatherDto> GetAsync(string city);
    }

    public class WeatherService : IWeatherService
    {
        private readonly IWeatherDownloader _weatherDownloader;
        private readonly IWeatherRepository _weatherRepository;
        private readonly IMapper _mapper;

        public WeatherService(
            IWeatherDownloader weatherDownloader, 
            IWeatherRepository weatherRepository,
            IMapper mapper)
        {
            _weatherDownloader = weatherDownloader;
            _weatherRepository = weatherRepository;
            _mapper = mapper;
        }

        public async Task<RefreshResponse> Refresh(string city)
        {
            var response = await _weatherDownloader.GetAsync(city);

            if (!response.IsSuccessful)
            {
                return HandleResponseError(response);
            }

            var weather = _mapper.Map<Weather>(response.Data);
            weather.DateTime = DateTime.Now;

            return await _weatherRepository.Add(weather) ? RefreshResponse.Ok : RefreshResponse.Other;
        }

        public async Task<WeatherDto> GetAsync(string city)
        {
            var weather =  await _weatherRepository
                .GetAsync(city.ToLower());
            return _mapper.Map<WeatherDto>(weather);
        }

        private RefreshResponse HandleResponseError(IRestResponse response) 
            => response.StatusCode == HttpStatusCode.NotFound ? RefreshResponse.UnknowCity : RefreshResponse.Other;
    }
}