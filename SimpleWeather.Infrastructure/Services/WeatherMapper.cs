using System.Linq;
using SimpleWeather.Data.Models;
using SimpleWeather.Infrastructure.Extensions;

namespace SimpleWeather.Infrastructure.Services
{
    public interface IWeatherMapper
    {
        Weather FromApiModel(WeatherApiModel model);
    }

    public class WeatherMapper : IWeatherMapper
    {
        public Weather FromApiModel(WeatherApiModel model)
        {
            return new Weather
            {
                City = model.Name,
                WeatherType = model.Weather.FirstOrDefault()?.Main,
                Temperature = model.Main.Temp,
                Pressure = model.Main.Pressure,
                Humidity = model.Main.Humidity,
                WindSpeed = model.Wind.Speed,
                WindDegree = model.Wind.Deg,
                CloudsPercentage = model.Clouds.All,
                Rain = model.Rain.DefaultIfNull(rain => rain.Rain3h),
                Snow = model.Snow.DefaultIfNull(snow => snow.Snow3h)
            };
        }
    }
}