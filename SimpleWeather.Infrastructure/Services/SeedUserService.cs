﻿using System.Threading.Tasks;
using Autofac;
using SimpleWeather.Infrastructure.Dto;

namespace SimpleWeather.Infrastructure.Services
{
    public class SeedUserService : IStartable
    {
        private readonly IUserService _userService;

        public SeedUserService(IUserService userService)
        {
            _userService = userService;
        }

        public async void Start()
        {
            await _userService.RegisterAdminAsync(new UserRegisterDto
            {
                Password = "admin",
                Name = "admin"
            });

            await _userService.RegisterUserAsync(new UserRegisterDto
            {
                Password = "user",
                Name = "user"
            });
        }
    }
}
