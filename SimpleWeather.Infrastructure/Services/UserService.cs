﻿using System.Threading.Tasks;
using SimpleWeather.Data.Models;
using SimpleWeather.Data.Repositories;
using SimpleWeather.Infrastructure.Dto;
using SimpleWeather.Infrastructure.Extensions;

namespace SimpleWeather.Infrastructure.Services
{
    public interface IUserService
    {
        Task<string> LoginAsync(UserLoginDto userLoginDto);
        Task<UserRegisterResponse> RegisterUserAsync(UserRegisterDto userRegisterDto);
        Task<UserRegisterResponse> RegisterAdminAsync(UserRegisterDto userRegisterDto);
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncryptService _encryptService;
        private readonly IJwtService _jwtService;

        public UserService(
            IUserRepository userRepository, 
            IEncryptService encryptService,
            IJwtService jwtService)
        {
            _userRepository = userRepository;
            _encryptService = encryptService;
            _jwtService = jwtService;
        }

        public async Task<string> LoginAsync(UserLoginDto userLoginDto)
        {
            var user = await _userRepository.GetByLoginAsync(userLoginDto.Login);
            if (user == null)
                return null;

            var hash = _encryptService.GetHash(userLoginDto.Password, user.Salt);

            return user.Hash == hash ? _jwtService.CreateToken(user) : null;
        }

        public async Task<UserRegisterResponse> RegisterAdminAsync(UserRegisterDto userRegisterDto)
            => await RegisterAsync(userRegisterDto, Role.Admin);

        public async Task<UserRegisterResponse> RegisterUserAsync(UserRegisterDto userRegisterDto)
            => await RegisterAsync(userRegisterDto, Role.User);

        private async Task<UserRegisterResponse> RegisterAsync(UserRegisterDto userRegisterDto, Role role)
        {
            var userExist = _userRepository.UserExist(userRegisterDto.Name);
            if (userExist)
                return UserRegisterResponse.UserExist;

            if (userRegisterDto.Password.IsNullOrEmpty())
                return UserRegisterResponse.PasswordEmpty;

            var salt = _encryptService.GetSalt(userRegisterDto.Password);
            var hash = _encryptService.GetHash(userRegisterDto.Password, salt);

            var user = new User(userRegisterDto.Name, hash, salt, role);

            await _userRepository.AddAsync(user);
            return UserRegisterResponse.Ok;
        }
    }
}
