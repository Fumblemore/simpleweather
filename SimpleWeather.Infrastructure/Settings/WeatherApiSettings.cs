namespace SimpleWeather.Infrastructure.Settings
{
    public class WeatherApiSettings
    {
        public string BaseUrl { get; set; }
        public string Resource { get; set; }
        public string CityParameter { get; set; }
        public string ApiKeyParameter { get; set; }
        public string ApiKeyValue { get; set; }
    }
}