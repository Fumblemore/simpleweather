using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleWeather.Infrastructure.Dto;
using SimpleWeather.Infrastructure.Services;

namespace SimpleWeather.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "Admin")]
    public class WeatherRefreshController : Controller
    {
        private readonly IWeatherService _weatherService;

        public WeatherRefreshController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [HttpPost("{city}")]
        public async Task<IActionResult> Post(string city)
        {
            var response = await _weatherService.Refresh(city);

            if (response == RefreshResponse.UnknowCity)
                return NotFound("Nie znaleziono danego miasta");

            if (response == RefreshResponse.Other)
                return StatusCode(500);

            return Created("api/weather", new object());
        }
    }
}