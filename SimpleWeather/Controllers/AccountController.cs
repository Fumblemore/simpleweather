﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleWeather.Infrastructure.Dto;
using SimpleWeather.Infrastructure.Services;

namespace SimpleWeather.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        //[HttpPost("register")]
        //public async Task<IActionResult> Register([FromBody]UserRegisterDto userRegisterDto)
        //{
        //    _userService.RegisterUserAsync()
        //}
        
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]UserLoginDto userLoginDto)
        {
            var token = await _userService.LoginAsync(userLoginDto);
            return Json(token);
        }
    }
}