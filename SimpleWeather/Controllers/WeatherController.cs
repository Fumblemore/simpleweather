using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleWeather.Infrastructure.Services;

namespace SimpleWeather.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    public class WeatherController : Controller
    {
        private readonly IWeatherService _weatherService;
        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [HttpGet("{city}")]
        public async Task<IActionResult> Get(string city)
        {
            return Json(await _weatherService.GetAsync(city));
        }
    }
}