export class WeatherModel {
  city: string;
  weatherType: string;
  temperature: number;
  pressure: number;
  humidity: number;
  windSpeed: number;
  windDegree: number;
  cloudsPercentage: number;
  snow: number;
  rain: number;
}
