import { Component, OnInit } from '@angular/core';
import {LoginService} from "../login.service";
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name: string;
  password: string;

  loginText:string;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    const result = this.loginService.login(this.name, this.password).subscribe(result => {
      if (result)
        this.router.navigate(['/weather']);
      else
        this.loginText = "Logowanie NIE powiodło się";
    });
  }

}
