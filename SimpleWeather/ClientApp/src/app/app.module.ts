import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { WeatherComponent } from './weather/weather.component';
import { WeatherService } from './weather.service';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    WeatherComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'weather', component: WeatherComponent },
      { path: 'login', component: LoginComponent }
    ]),
  ],
  providers: [WeatherService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
