import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { WeatherModel } from '../../models/WeatherModel';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  weather: WeatherModel;
  selectedCity: string;

  weatherCallback: string;
  watherRefreshCallback: string;

  constructor(private weatherService: WeatherService, private loginService: LoginService) { }

  getWeather() {  
    this.weatherService.getWeather(this.selectedCity).subscribe(w => {
      if (w) {
        this.weatherCallback = "";
        this.weather = w;
      } else {
        this.weatherCallback = "Nie znaleziono danego miasta";
        this.weather = null;
      }
    });
  }

  refreshWeather() {
    this.weatherService.refreshWeather(this.selectedCity)
      .subscribe(
        s => this.weatherCallback = "Odświeżenie powiodło się!",
        e => {
          this.weatherCallback = "Odświeżenie NIE powiodło się!";
          this.weather = null;
        }
      );
  }

  canShowWeather(): boolean {
    let role = this.loginService.getRole();
    return role === "User" || role === "Admin";
  }

  canRefreshWeather(): boolean {
    return this.loginService.getRole() === "Admin";
  }


  ngOnInit() {
  }

}
