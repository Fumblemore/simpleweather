import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WeatherModel } from '../models/WeatherModel'
import { Observable } from 'rxjs/Observable';
import { LoginService } from './login.service';

@Injectable()
export class WeatherService {
  private weatherUrl = 'api/weather';
  private weatherRefreshUrl = 'api/weatherrefresh';

  constructor(private http: HttpClient,
    private loginService: LoginService) { }

  getWeather(city: string): Observable<WeatherModel> {
    console.log(this.loginService.token);
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'Bearer ' +  this.loginService.token})
    };
    return this.http.get<WeatherModel>(this.weatherUrl + "/" + city, httpOptions);
  }

  refreshWeather(city: string): Observable<Object> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        .append('Authorization', 'Bearer ' + this.loginService.token)
    };

    return this.http.post(this.weatherRefreshUrl + "/" + city, "", httpOptions);
  }
}
