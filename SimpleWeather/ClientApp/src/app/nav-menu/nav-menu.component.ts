import { Component } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;

  constructor(private loginService: LoginService, private router: Router) {  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  canShowWeather(): boolean {
    let role = this.loginService.getRole();
    return role === "User" || role === "Admin";
  }

  logOut() {
    this.loginService.logOut();
    this.router.navigate(['/login']);
  }
}
