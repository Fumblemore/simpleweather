import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {

  token: string;
  role: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('currentToken');
    this.role = localStorage.getItem('currentRole');
  }

  login(login: string, password: string): Observable<boolean> {
    return this.http.post("api/account/login", { login, password })
      .map((token: string) => {
        if (token) {

          this.role = this.getRoleFromToken(token);

          this.token = token;
          localStorage.setItem('currentToken', token);
          localStorage.setItem('currentRole', this.role);
          return true;
        }

        this.token = null;
        this.role = null;
        return false;
      });
  }

  getRole(): string {
     return this.role;
  }

  logOut() {
    this.token = null;
    this.role = null;
  }

  private getRoleFromToken(token: string): string {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64))["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
  }

}
